console.log ("Hello World")

//Item no. 3 & 4
let number = 2
    getCube = number ** 3;
    console.log (`The cube of ${number} is ${getCube} `)

    
// Item no. 5 & 6

let Address = ['258 Washington Ave. NW', 'California', '90011'];
let [StreetName, State, Postal] = Address;
console.log(`I live at ${StreetName} ${State} ${Postal}`)

// Item No.7 & 8

let Animal = {
    name: "Lolong",
    kind: "Saltwater Crocodile",
    weight: '1075 kg',
    length: '20 ft 3 in',
}

let {name, kind, weight, length} = Animal;
    console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${length} `)

// Item No. 9 & 10

let numbers = [1, 2, 3, 4, 5, 15];

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach((number) => {
	console.log(`${number}`);
})

//Item No. 11

let reduceNumbers = [2, 4, 6, 8, 10];
const add = (a, b, c, d, e) => (a + b + c + d + e);
let total = add(2,4,6,8,10 )
console.log(`The sum of the numbers is ${total}`)



//Item No. 12 & 13

class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let NewDog = new Dog ("Frankie", 5, "Miniature Dachshund");
 
console.log (NewDog)
